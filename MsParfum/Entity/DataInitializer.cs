﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MsParfum.Entity
{
    public class DataInitializer:DropCreateDatabaseIfModelChanges<DataContext>
    {
        protected override void Seed(DataContext context)
        {
            var kategoriler = new List<Kategori>()
            {
                new Kategori() { Adi = "Hugo Boss", Aciklama = "Efsane" ,KategoriId=1},
                new Kategori() { Adi = "Armani", Aciklama = "Zirve",KategoriId=2},
                new Kategori() { Adi = "Kenzo Home", Aciklama = "Sınırları Zorla",KategoriId=3},
                new Kategori() { Adi = "Bvlgari", Aciklama = "Cürretkar",KategoriId=4},
            };

            foreach (var kategori in kategoriler)
            {
                context.Kategoris.Add(kategori);
            }

            context.SaveChanges();

            var urunler = new List<Urun>()
            {
                new Urun() { Adi = "Hugo Boss-Bottled", Aciklama = "Baştan Çıkarıcı", Fiyat = 500,KategoriId=1,Stok=100},
                new Urun() { Adi = "Hugo Boss-Intense", Aciklama = "Şaşırtıcı", Fiyat = 500,KategoriId=1,Stok=100},
                new Urun() { Adi = "Hugo Boss-Mix", Aciklama = "Alışılmışın Dışında", Fiyat = 500,KategoriId=1,Stok=100},
                new Urun() { Adi = "Armani-Black Code", Aciklama = "Muazzam", Fiyat = 500,KategoriId=2,Stok=100},
                new Urun() { Adi = "Armani-Code", Aciklama = "Standardın Dışında", Fiyat = 500,KategoriId=2,Stok=100},
                new Urun() { Adi = "Kenzo Home-Pour Home", Aciklama = "Fresh", Fiyat = 500,KategoriId=3,Stok=100},
                new Urun() { Adi = "Kenzo Home-Homme", Aciklama = "Cazibe", Fiyat = 500,KategoriId=3,Stok=100},
                new Urun() { Adi = "Bvlgari-Aqva", Aciklama = "Enfes", Fiyat = 500,KategoriId=4,Stok=100},
                new Urun() { Adi = "Bvlgari-Blue", Aciklama = "Sınırları Zorla", Fiyat = 500,KategoriId=4,Stok=100},
                new Urun() { Adi = "Bvlgari-Mogma", Aciklama = "Çekici", Fiyat = 500,KategoriId=4,Stok=100},
                new Urun() { Adi = "Armani-asd", Aciklama = "qwe", Fiyat = 200,KategoriId=2,Stok=100},

            };

            foreach (var urun in urunler)
            {
                context.Uruns.Add(urun);
            }

            context.SaveChanges();
          

            base.Seed(context);

        }
    }
}