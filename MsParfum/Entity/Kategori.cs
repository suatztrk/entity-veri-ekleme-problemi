﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MsParfum.Entity
{
    public class Kategori
    {
        [Key]
        public int Id { get; set; }
        public string Adi { get; set; }
        public string Aciklama { get; set; }
        public int KategoriId { get; set; }
        public List<Urun> Uruns { get; set; }
    }
}