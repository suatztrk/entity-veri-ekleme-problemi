﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MsParfum.Entity
{
    public class Urun
    {
        [Key]
        public int Id { get; set; }
        public string Adi { get; set; }
        public string Aciklama { get; set; }
        public double Fiyat { get; set; }
        public byte Resim { get; set; }
        public int KategoriId { get; set; }
        public int Stok { get; set; }
        public Kategori Kategori { get; set; }

    }

}