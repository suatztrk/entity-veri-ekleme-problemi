﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MsParfum.Entity;

namespace MsParfum.Controllers
{
    public class HomeController : Controller
    {
        DataContext _context=new DataContext();
        // GET: Home
        public ActionResult Index()
        {
            
            return View(_context.Uruns.ToList());
        }

        public ActionResult Create()
        {
            return View();
        }

    }
}